How it works
============

Folder fr/ stores the current theme in use at https://fr.squat.net
You can edit it or submit pull requests.

To update the actual on production theme, one should clone the repo localy and
scp the files to the production server.


You can also create new themes in this repo (just put them in another folder
next to fr/)